from context import requests
from twisted.trial import unittest
from txrequests import Session
from twisted.internet import defer
import collections


class TestBasicRest(unittest.TestCase):

    URL1 = 'http://virtserver.swaggerhub.com/romradz/GNInvAPI/1.0.0/resourceInventoryManagement/logicalResources'
    URL2 = 'http://virtserver.swaggerhub.com/romradz/GNInvAPI/1.0.0/resourceInventoryManagement/networks'

    @defer.inlineCallbacks
    def test_start(self):
        with Session() as session:
            response1 = yield session.get(TestBasicRest.URL1)
            self.assertIsNotNone(response1)
            self.assertEqual(200, response1.status_code)
            json = response1.json()
            self.assertTrue(isinstance(json,collections.Iterable))
            response2 = yield session.get(TestBasicRest.URL2)
            self.assertIsNotNone(response2)
            self.assertEqual(200, response2.status_code)
            json = response2.json()
            self.assertTrue(isinstance(json,collections.Iterable))



if __name__ == '__main__':
    unittest.main()
