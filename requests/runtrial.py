# run twisted unittests, for example from the Intellij IDE/Intellij
# Note: this is a copy from /usr/local/bin/trial
import re
import sys

from twisted.scripts.trial import run

if __name__ == '__main__':
    sys.argv[0] = re.sub(r'(-script\.pyw?|\.exe)?$', '', sys.argv[0])
    sys.exit(run())
