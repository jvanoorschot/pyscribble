# Demonstrating Twistar in the Twisted framework

The tests/samples require a postgresql database with the name 'pyscrible' to exist, 
owned by user (password userpwd)

```
sudo -u postgres createuser -P -s -e user
psql postgres user
postgresql=# CREATE DATABASE pyscribble;
psql -U user -d pyscribble < docs/schema.sql

```
