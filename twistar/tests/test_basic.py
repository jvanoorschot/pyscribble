from context import orm
from twisted.enterprise import adbapi
from twisted.internet import defer, reactor
from twisted.internet.defer import inlineCallbacks
from twistar.registry import Registry
from twisted.trial import unittest
from orm.objects import User


class TestBasic(unittest.TestCase):

    def setUp(self):
        Registry.DBPOOL = adbapi.ConnectionPool('psycopg2', user="user", password="userpwd", database="pyscribble")

    def setupDB(self):
        return User.deleteAll()

    def cleanupDB(self):
        return User.deleteAll()

    def sleep(self,secs):
        d = defer.Deferred()
        reactor.callLater(secs, d.callback, None)
        return d

    @inlineCallbacks
    def test_null(self):
        """ Demonstrate inlineCallBack usage with dummy twisted sleep function"""
        def sleepDone(t):
            self.assertTrue(True)
        yield self.sleep(0).addCallback(sleepDone)

    @inlineCallbacks
    def test_dbhandling(self):
        """Test and demonstrate the setupDB/cleanupDB functionality"""
        yield self.setupDB()
        u = User(first_name="John", last_name="Smith", age=25)
        yield u.save()
        count = yield User.count()
        self.assertTrue(count == 1)
        yield self.cleanupDB()
        count = yield User.count()
        self.assertTrue(count == 0)

    def test_start(self):
        """ Execute simplest twistar logic. See docs/Readme.md for prereqs"""
        def done(user):
            self.assertTrue(user.id >= 0)
        u = User()
        u.first_name = "John"
        u.last_name = "Smith"
        u.age = 25
        return u.save().addCallback(done)




if __name__ == '__main__':
    unittest.main()