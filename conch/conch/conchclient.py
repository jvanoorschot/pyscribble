# see http://twistedmatrix.com/documents/current/conch/howto/conch_client.html
# see http://twistedmatrix.com/documents/current/core/howto/defer-intro.html
import os, getpass
import configparser
from binascii import a2b_base64
from twisted.internet.defer import Deferred
from twisted.internet.protocol import Factory, Protocol
from twisted.internet.endpoints import UNIXClientEndpoint
from twisted.conch.endpoints import SSHCommandClientEndpoint
from twisted.conch.ssh.keys import EncryptedKeyError, Key
from twisted.python.filepath import FilePath
from twisted.internet.defer import inlineCallbacks, returnValue
from twisted.conch.client.knownhosts import KnownHostsFile


class MainBashProtocol(Protocol):
    finished = None

    def connectionMade(self):
        self.finished = Deferred()

    def dataReceived(self, data):
        pass

    def connectionLost(self, reason):
        self.finished.callback(None)

    def stop(self):
        self.transport.loseConnection()
        self.finished.callback(self)


class FindProtocol(Protocol):
    finished = None
    path = None
    lines = []
    buffer = ""
    deferred = None

    def __init__(self):
        self.finished = Deferred()
        self.path = None

    def connectionMade(self):
        self.finished = Deferred()

    def connectionLost(self, reason):
        if not self.deferred is None:
            self.deferred.callback(self.buffer)
            self.deferred = None
        self.finished.callback(self.path)

    def dataReceived(self, data):
        data = self.buffer + data
        idx = data.find('\n')
        while idx >= 0:
            line = data[:idx]
            data = data[idx+1:]
            self.lines.append(line)
            idx = data.find('\n')
        self.buffer = data
        while self.deferred is not None and len(self.lines) > 0:
            self.sendlines()

    def line(self):
        if self.deferred is None:
            self.deferred = Deferred()
        return self.sendlines()

    def sendlines(self):
        if len(self.lines) > 0:
            d = self.deferred
            self.deferred = None
            line = self.lines.pop()
            d.callback(line)
            return d
        else:
            return self.deferred


def startfind(proto, path):
    cmd = ("/usr/bin/find " + path).encode('utf-8')
    endpoint = SSHCommandClientEndpoint.existingConnection(proto.transport.conn, cmd)
    factory = Factory()
    factory.protocol = FindProtocol
    finished = Deferred()

    def done(path):
        finished.callback(path)

    def connected(proto2):
        proto2.path=path
        proto2.finished.addCallback(done)
        readlines(path, proto2.line)

    d = endpoint.connect(factory)
    d.addCallback(connected)
    return finished

@inlineCallbacks
def readlines(path, linereader):
    print "readlines"
    done = False
    while not done:
        line = yield linereader()
        if line is None:
            done = True
        else:
            print "{}:{}".format(path, line)

if __name__ == '__main__':
    from twisted.internet.task import react

    def readKey(path, passphrase):
        try:
            return Key.fromFile(path)
        except EncryptedKeyError:
            # passphrase = getpass.getpass("%r keyphrase: " % (path,))
            return Key.fromFile(path, passphrase=passphrase)


    def main(reactor, *argv):
        # configure our app
        config = configparser.ConfigParser()
        configfile = os.path.expanduser("~/.pyscribble")
        if(os.path.exists(configfile)):
            config.read(configfile)
        host = config.get("conch", "host", fallback='www.google.com')
        port = config.get("conch", "port", fallback='22')
        user = config.get("conch", "user", fallback='me')
        password = config.get("conch", "password", fallback='secret')
        identity = config.get("conch", "identity", fallback='~/.ssh/id_rsa')
        # create credentials (ssh-agent or identity file)
        if "SSH_AUTH_SOCK" in os.environ:
            ae = UNIXClientEndpoint(
                reactor, os.environ["SSH_AUTH_SOCK"])
            ks=[]
            khf=None
        else:
            ae = None
            keyscan = (
                'AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBFm/GuXABJljRWQAKSG/1cP25nmY'
                'CpuOxy6D2IimmZ/3MuDhLH3p5QeEItKkfoMfv3b0rtM0DgoGWqZIY+bhs2M=')
            key = Key.fromString(a2b_base64(keyscan))
            # khf = KnownHostsFile.fromPath(FilePath("knownhosts"))
            khf = KnownHostsFile(FilePath("knownhosts"))
            he = khf.addHostKey(b"www.janvanoorschot.nl", key)
            ks=[]
            if identity:
                keyPath = identity
                if os.path.exists(keyPath):
                    ks.append(readKey(keyPath, password))
        # start the main bash which only opens the SSH connection
        endpoint = SSHCommandClientEndpoint.newConnection(
            reactor,
            "/bin/bash",
            user,
            host,
            keys=ks,
            agentEndpoint=ae,
            knownHosts=khf,
            port=int(port))
        factory = Factory()
        factory.protocol = MainBashProtocol
        alldone = Deferred()
        # use the ssh connection as carrier for several find commands
        paths = ["/etc", "/home"]

        def done(d):
            alldone.callback(d)

        def connected(proto):

            def decrease(path):
                paths.remove(path)
                if len(paths) == 0:
                    proto.stop()

            for path in paths:
                d = startfind(proto,path)
                d.addCallback(decrease)

            proto.finished.addCallback(done)
            return proto.finished

        d = endpoint.connect(factory)
        d.addCallback(connected)
        return alldone

    react(main)
