from base64 import b64encode
import json
import configparser
import os.path
from pprint import pprint
from twisted.web.http_headers import Headers
from twisted.web.client import Agent, readBody
from twisted.internet.ssl import ClientContextFactory
from twisted.internet.task import react

API_CMD = b"GET"
API_URL = b"/posts/1"
API_ACCEPT_HEADER = b"application/json"

class RestClient:

    def __init__(self, agent, user, password):
        self.agent = agent
        self.authorization = b64encode(b"%s:%s" % (user.encode('latin-1'),password.encode('latin-1')))

    def cbResponse(self, response):
        print("************** header ******************")
        pprint(vars(response))
        d = readBody(response)
        d.addCallback(self.handleBody)
        return d

    def handleBody(self, j):
        print("************** body ******************")
        body = json.loads(j.decode('utf-8'))
        pprint(body)

    def cbError(self, error):
        pprint(vars(error))

    def get(self, uri, body=None):
        headers = Headers({
            b'authorization':[b"Basic " + self.authorization],
            b"Accept": [b"application/json"]
        })
        d = self.agent.request(b"GET", uri.encode('latin-1'), headers)
        d.addCallbacks(self.cbResponse, self.cbError)
        return d

if __name__ == '__main__':


    class WebClientContextFactory(ClientContextFactory):

        def getContext(self, hostname, port):
            return ClientContextFactory.getContext(self)

    def main(reactor):
        config = configparser.ConfigParser({
            'uri': 'http://jsonplaceholder.typicode.com/posts/1',
            'user': 'me',
            'password': 'secret'})
        configfile = os.path.expanduser("~/.pyscribble")
        if(os.path.exists(configfile)):
            config.read(configfile)
        uri = config.get("rest", "uri")
        user = config.get("rest", "user")
        password = config.get("rest", "password")
        contextFactory = WebClientContextFactory()
        agent = Agent(reactor,contextFactory)
        client = RestClient(agent, user, password)
        return client.get(uri)

    react(main)
